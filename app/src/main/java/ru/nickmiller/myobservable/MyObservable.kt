package ru.nickmiller.myobservable

import android.os.Handler
import android.os.Looper
import android.util.Log


class MyObservable<T> private constructor(private val callable: () -> T) {
    private val MY_OBSERVABLE = "MyObservable"
    private val SEND_RESULT = 1
    private var workLooper: Looper? = null
    private var targetLooper: Looper? = null

    companion object {
        fun <T> from(callable: (() -> T)?) =
            if (callable == null) throw NullPointerException("Callable is null")
            else MyObservable(callable)
    }

    fun subscribeOn(looper: Looper) = this.apply { workLooper = looper }

    fun observeOn(looper: Looper?) = this.apply { targetLooper = looper }

    fun subscribe(callback: (data: T) -> Unit) {
        if (workLooper == null) workLooper = Looper.myLooper()
        if (targetLooper == null) targetLooper = workLooper

        if (targetLooper == null) callback(execCallable())  // Worker and Target are not associated with a Looper. Use 'subscribe' thread.
        else {
            val targetHandler = Handler(targetLooper) { // Use provided Looper for Target
                Log.d(MY_OBSERVABLE, "Result comes into thread '${Thread.currentThread().name}'")
                if (it.what == SEND_RESULT) callback(it.obj as T)
                true
            }

            if (workLooper == null) sendDataIntoHandler(targetHandler, execCallable())  // Use 'subscribe' thread for Worker
            else Handler(workLooper).post {  // Use provided Looper for Callable
                sendDataIntoHandler(targetHandler, execCallable())
            }
        }
    }

    private fun execCallable(): T {
        Log.d(MY_OBSERVABLE, "Execute callable on thread '${Thread.currentThread().name}'")
        return callable.invoke()
    }

    private fun sendDataIntoHandler(handler: Handler, data: T) {
        val message = handler.obtainMessage(SEND_RESULT, data)
        handler.sendMessage(message)
    }
}