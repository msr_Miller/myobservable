package ru.nickmiller.myobservable

import android.os.Bundle
import android.os.HandlerThread
import android.support.v7.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val worker = HandlerThread("worker")
        worker.start()

        MyObservable.from { "Hello World" }
            .subscribeOn(worker.looper)
            .observeOn(mainLooper)
            .subscribe {
                println("Message $it from thread ${Thread.currentThread().name}")
            }
    }
}
